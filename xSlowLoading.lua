
-- ------------------------------------------
-- xSlowLoading
--   by: Xsear
-- ------------------------------------------

require "math"
require "table"
require "unicode"
require "lib/lib_Debug"
require "lib/lib_Callback2"
require "lib/lib_ChatLib"
require "lib/lib_InterfaceOptions"
require "lib/lib_Slash"


-- ------------------------------------------
-- CONSTANTS
-- ------------------------------------------

c_AddonInfo = {
    component = select(1, Component.GetInfo()),
    name = select(2, Component.GetInfo()),
    release  = "2016-05-15",
    version = "1.0",
    patch = "prod-1962",
    save = 1,
}


-- ------------------------------------------
-- GLOBALS
-- ------------------------------------------

g_MatchLoadingComplete = false
g_MatchLoadingState = nil
g_PlayerName = ""

-- ------------------------------------------
-- OPTIONS
-- ------------------------------------------

g_Loaded = false
g_Options = {}
g_Options.Enabled = false
g_Options.Debug = false
g_Options.OnlyWhenLeader = true
g_Options.StartupDelayFactor = 8
g_Options.FinalDelayFactor = 6
g_Options.ConnectedDelayFactor = 4
g_Options.LoadedDelayFactor = 1

-- InterfaceOptions Configuration
do
    InterfaceOptions.NotifyOnLoaded(true)
    InterfaceOptions.NotifyOnDefaults(true)
    InterfaceOptions.NotifyOnDisplay(true)
    InterfaceOptions.SaveVersion(c_AddonInfo.save)

    InterfaceOptions.AddCheckBox({id = "Enabled", label = "Enable addon", default = g_Options.Enabled})
    InterfaceOptions.AddCheckBox({id = "Debug", label = "Enable debug", tooltip = "Log messages into the console that are helpful to the addon creator in order to track down problems.", default = g_Options.Debug})
    InterfaceOptions.AddCheckBox({id = "OnlyWhenLeader", label = "Only when group leader", default = g_Options.OnlyWhenLeader})
    InterfaceOptions.AddSlider({id = "StartupDelayFactor", label = "StartupDelayMulti", tooltip = "Increase to slow further", default = g_Options.StartupDelayFactor, min = 1, max = 50, inc = 1, suffix = "x"})
    InterfaceOptions.AddSlider({id = "FinalDelayFactor", label = "FinalDelayMulti", tooltip = "Increase to slow further", default = g_Options.FinalDelayFactor, min = 1, max = 50, inc = 1, suffix = "x"})
    InterfaceOptions.AddSlider({id = "ConnectedDelayFactor", label = "ConnectedDelayMulti", tooltip = "Increase to slow further", default = g_Options.ConnectedDelayFactor, min = 1, max = 50, inc = 1, suffix = "x"})
    InterfaceOptions.AddSlider({id = "LoadedDelayFactor", label = "LoadedDelayMulti", tooltip = "Increase to slow further", default = g_Options.LoadedDelayFactor, min = 1, max = 50, inc = 1, suffix = "x"})
end

-- InterfaceOptions Updates
function OnOptionChanged(id, value)

    -- InterfaceOptions Notifications
    if id == "__LOADED" then
        OnOptionsLoaded()
        return
    elseif id == '__DEFAULT' then
        return
    elseif id == '__DISPLAY' then
        return
    end

    -- Update value
    g_Options[id] = value

    -- Debug
    if id == "Debug" then
        Component.SaveSetting("Debug", value)
        Debug.EnableLogging(value)
        return
    end    
end

-- InterfaceOptions Loaded
function OnOptionsLoaded()
    g_Loaded = true
end


-- ------------------------------------------
-- LOADING EVENTS
-- ------------------------------------------

function OnComponentLoad(args)
    -- Debug
    Debug.EnableLogging(Component.GetSetting("Debug"))

    -- Options
    InterfaceOptions.SetCallbackFunc(OnOptionChanged)
end

function OnMatchLoadingStart(args)
    Debug.Event(args)
    g_MatchLoadingComplete = false -- Reset hard exit flag
    if g_Loaded and g_Options.Enabled then
        if g_Options.OnlyWhenLeader and not IsGroupLeader() then Debug.Log("Ignoring because not group leader") return end
        g_PlayerName = Player.GetInfo() -- Used to ensure we don't wait for ourselves to finish loading :D
        g_MatchLoadingState = Game.GetMatchLoadState() -- Init state
        MultiOccupyCPU(g_Options.StartupDelayFactor) -- StartupDelayFactor
        SmartDelay()
    end
end


function OnMatchLoadingChange(args)
    Debug.Event(args)
    if g_MatchLoadingState then
        for _, player in pairs(g_MatchLoadingState.Players) do
            if namecompare(player.PlayerName, args.PlayerName) then
                player.Connected = args.Connected
                player.Loaded = args.Loaded
                Debug.Table("Updated g_MatchLoadingState", g_MatchLoadingState)
                break
            end
        end
    end
end

function OnMatchLoadingComplete(args)
    Debug.Event(args)
    g_MatchLoadingComplete = true -- Raise hard exit flag
end

function SmartDelay()
    Debug.Log("SmartDelay")
    
    local numberOfPlayers = #g_MatchLoadingState.Players
    local allPlayersLoaded = true
    local allPlayersConnected = true
    if numberOfPlayers > 1 then
        for i = 1, numberOfPlayers do
            local player = g_MatchLoadingState.Players[i]
            if not player.Connected and not namecompare(player.PlayerName, g_PlayerName) then
                allPlayersConnected = false
                Debug.Log(player.PlayerName .. " has not connected")
                break
            elseif not player.Loaded and not namecompare(player.PlayerName, g_PlayerName) then
                allPlayersLoaded = false
                Debug.Log(player.PlayerName .. " has not finished loading")
                break
            end
        end
    end

    -- Check if loading has ended
    if g_MatchLoadingComplete then
        -- Okay, just quit delaying.
        return

    -- Okay, we're still loading
    else
        -- Check for all green
        if allPlayersConnected and allPlayersLoaded then
            MultiOccupyCPU(g_Options.FinalDelayFactor) -- FinalDelayFactor
            Debug.Table("FIN SmartDelay", {allPlayersLoaded=allPlayersLoaded,g_MatchLoadingComplete=g_MatchLoadingComplete})

        -- Some people are not ready yet
        else
            if not allPlayersConnected then
                MultiOccupyCPU(g_Options.ConnectedDelayFactor) -- ConnectedDelayFactor
            elseif not allPlayersLoaded then
                MultiOccupyCPU(g_Options.LoadedDelayFactor) -- LoadedDelayFactor
            end
            Callback2.FireAndForget(SmartDelay, nil, 0.1)
        end
    end
end


-- ------------------------------------------
-- GENERAL FUNCTIONS
-- ------------------------------------------

function MultiOccupyCPU(multi)
    for i = 1, multi do
        OccupyCPU()
    end
end

function OccupyCPU()
    Debug.Log("OccupyCPU")
    local maxNum = 300000

    for num = 1, maxNum do
        local itemInfo = Game.GetItemInfoByType(num)
        if itemInfo and not _table.empty(itemInfo) then
            -- I'll be your scaPEGoat if I can~
        end
    end
end

function IsGroupLeader()
    local roster = Squad.GetRoster()
    return roster.is_mine
end


-- ------------------------------------------
-- UTILITY/RETURN FUNCTIONS
-- ------------------------------------------

function Output(text)
    local args = {
        text = c_OutputPrefix .. tostring(text),
    }

    ChatLib.SystemMessage(args);
end

function _table.empty(table)
    if not table or next(table) == nil then
       return true
    end
    return false
end

